﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CSchrieverHexagonGrid
{

	public class HexagonCell : MonoBehaviour
	{
		public GameObject cellObject;

		public HCoordinates coordinates;
		public HexagonChunk chunk;
		public List<HexagonCell> neighbors;

		public Color color;
		
		public void OnDelete()
		{

		}		

		public void assignCellObject(GameObject obj)
		{
			cellObject = obj;
		}

		public List<HexagonCell> GetAdjacent()
		{
			return neighbors;
		}

		public void AddNeighbor(HexagonCell cell)
		{
			neighbors.Add(cell);
		}

		//void Refresh()
		//{
		//	if (chunk)
		//	{
		//		chunk.Refresh();

		//		//if (transform.childCount > 0)
		//		//{
		//		//	for (int i = 0; i < transform.childCount; i++)
		//		//	{
		//		//		Destroy(transform.GetChild(i).gameObject);
		//		//	}
		//		//}
		//		//for (int i = 0; i < neighbors.Length; i++)
		//		//{
		//		//	HexCell neighbor = neighbors[i];
		//		//	if (neighbor != null && neighbor.chunk != chunk)
		//		//	{
		//		//		neighbor.chunk.Refresh();
		//		//	}
		//		//}
		//	}
		//}
	}
}