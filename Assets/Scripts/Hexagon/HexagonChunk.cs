﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CSchrieverHexagonGrid
{

	public class HexagonChunk : MonoBehaviour
	{
		public HexagonCell[] cells;

		HexagonMesh hexMesh;
		Canvas gridCanvas;

		void Awake()
		{
			gridCanvas = GetComponentInChildren<Canvas>();
			hexMesh = GetComponentInChildren<HexagonMesh>();

			cells = new HexagonCell[HexMetrics.chunkSizeX * HexMetrics.chunkSizeZ];
		}

		public void AddCell(int index, GameObject cell)
		{
			HexagonCell cellHexCell = cell.GetComponent<HexagonCell>();
			cells[index] = cellHexCell;
			cellHexCell.chunk = this;
			cellHexCell.transform.SetParent(transform, false);
			//cellHexCell.uiRect.SetParent(gridCanvas.transform, false);
		}

		public void Refresh()
		{
			hexMesh.Triangulate(cells);
		}
	}
}