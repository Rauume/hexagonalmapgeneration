﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CSchrieverHexagonGrid
{
#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(HCoordinates))]
	public class HexCoordinatesDrawer : PropertyDrawer
	{
		public override void OnGUI(
			Rect position, SerializedProperty property, GUIContent label
		)
		{
			HCoordinates coordinates = new HCoordinates(
				property.FindPropertyRelative("x").intValue,
				property.FindPropertyRelative("z").intValue
			);

			position = EditorGUI.PrefixLabel(position, label);
			GUI.Label(position, coordinates.ToString());
		}
	}
#endif


	[System.Serializable]
	public struct HCoordinates
	{
		[SerializeField]
		private int x, z;

		public int X {
			get {
				return x;
			}
		}

		public int Z {
			get {
				return z;
			}
		}

		public HCoordinates(int x, int z)
		{
			this.x = x;
			this.z = z;
		}

		public static HCoordinates FromOffsetCoordinates(int x, int z)
		{
			return new HCoordinates(x - z / 2, z);
		}

		public int Y {
			get {
				return -X - Z;
			}
		}

		public override string ToString()
		{
			return "(" + X.ToString() + ", " + Y.ToString() + ", " + Z.ToString() + ")";
		}

		public string ToStringOnSeparateLines()
		{
			return X.ToString() + "\n" + Y.ToString() + "\n" + Z.ToString();
		}

		public Vector3Int ToVector()
		{
			return new Vector3Int(X, Y, Z);
		}

		public static HCoordinates FromPosition(Vector3 position)
		{
			//use rounding to grab the correct coords, will rewrite later...
			float x = position.x / (HexMetrics.innerRadius * 2f);
			float y = -x;

			float offset = position.z / (HexMetrics.outerRadius * 3f);
			x -= offset;
			y -= offset;

			int iX = Mathf.RoundToInt(x);
			int iY = Mathf.RoundToInt(y);
			int iZ = Mathf.RoundToInt(-x - y);

			if (iX + iY + iZ != 0)
			{
				float dX = Mathf.Abs(x - iX);
				float dY = Mathf.Abs(y - iY);
				float dZ = Mathf.Abs(-x - y - iZ);

				if (dX > dY && dX > dZ)
				{
					iX = -iY - iZ;
				}
				else if (dZ > dY)
				{
					iZ = -iX - iY;
				}
			}

			return new HCoordinates(iX, iZ);
		}
	}
}