﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CSchrieverHexagonGrid
{

	public class HexagonBaseGen : MonoBehaviour
	{

		Mesh hexMesh;
		MeshCollider meshCollider;
		List<Vector3> vertices;
		List<int> triangles;
		List<Color> colors;

		// Start is called before the first frame update
		void Start()
		{
			GetComponent<MeshFilter>().mesh = hexMesh = new Mesh();
			meshCollider = gameObject.AddComponent<MeshCollider>();
			hexMesh.name = "Hex Mesh";
			vertices = new List<Vector3>();
			triangles = new List<int>();
			colors = new List<Color>();


			for (int i = 0; i < 6; i++)
			{
				AddTriangle(
				Vector3.zero,
				HexMetrics.corners[i],
				HexMetrics.corners[i + 1]
				);

				AddTriangleColor(Color.green);
			}

			hexMesh.vertices = vertices.ToArray();
			hexMesh.triangles = triangles.ToArray();
			hexMesh.colors = colors.ToArray();
			hexMesh.RecalculateNormals();

			meshCollider.sharedMesh = hexMesh;
		}


		void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3)
		{
			int vertexIndex = vertices.Count;
			vertices.Add(v1);
			vertices.Add(v2);
			vertices.Add(v3);
			triangles.Add(vertexIndex);
			triangles.Add(vertexIndex + 1);
			triangles.Add(vertexIndex + 2);
		}

		void AddTriangleColor(Color color)
		{
			colors.Add(color);
			colors.Add(color);
			colors.Add(color);
		}
	}
}