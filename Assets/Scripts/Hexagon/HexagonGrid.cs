﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace CSchrieverHexagonGrid
{
	public class HexagonGrid : MonoBehaviour
	{

		public int chunkCountX = 4, chunkCountZ = 3; //defaults, remember that...
		private int cellCountX, cellCountZ;
		public static int width, height;

		public GameObject HexCellPrefab;
		public HexagonCell[] cells;

		public HexagonChunk chunkPrefab;
		HexagonChunk[] chunks = new HexagonChunk[0];

		public void CreateGrid(int ChunkCountX, int ChunkCountZ)
		{
			//makes the chunks, assigns the cells to the chunks,
			//and connects them as neigbours.
			chunkCountX = ChunkCountX;
			chunkCountZ = ChunkCountZ;

			cellCountX = chunkCountX * HexMetrics.chunkSizeX;
			cellCountZ = chunkCountZ * HexMetrics.chunkSizeZ;
			width = chunkCountX * HexMetrics.chunkSizeX;
			height = chunkCountZ * HexMetrics.chunkSizeZ;

			CreateChunks();
			CreateCells();
			ConnectCells();

			Debug.Log("Cell Count: " + cells.Length);
			Debug.Log(width + "," + height);
		}

		void CreateChunks()
		{
			chunks = new HexagonChunk[chunkCountX * chunkCountZ];

			for (int z = 0, i = 0; z < chunkCountZ; z++)
			{
				for (int x = 0; x < chunkCountX; x++)
				{
					HexagonChunk chunk = chunks[i++] = Instantiate(chunkPrefab);
					chunk.transform.SetParent(transform);
				}
			}
		}

		void CreateCells()
		{
			cells = new HexagonCell[cellCountZ * cellCountX];

			for (int z = 0, i = 0; z < cellCountZ; z++)
			{
				for (int x = 0; x < cellCountX; x++)
				{
					CreateCell(x, z, i++);
				}
			}
		}

		void CreateCell(int x, int z, int i)
		{
			Vector3 position;
			position.x = (x + z * 0.5f - z / 2) * (HexMetrics.innerRadius * 2f);
			position.y = 0f;
			position.z = z * (HexMetrics.outerRadius * 1.5f);

			GameObject HexCell = Instantiate(HexCellPrefab);

			HexagonCell cell = cells[i] = HexCell.GetComponent<HexagonCell>();
			cell.transform.localPosition = position;
			cell.coordinates = HCoordinates.FromOffsetCoordinates(x, z);

			//messy, but shows the grid possition if a canvas exists.
			try
			{
				Canvas gridCanvas = HexCell.GetComponentInChildren<Canvas>();
				Text label = HexCell.GetComponentInChildren<Text>();
				label.rectTransform.anchoredPosition = new Vector2(position.x, position.z);
				label.transform.localPosition = Vector3.zero;
				label.text = cell.coordinates.ToStringOnSeparateLines();
			}
			catch {}

			AddCellToChunk(x, z, HexCell);
		}

		public void RefreshCells()
		{
			foreach (HexagonChunk chunk in chunks)
			{
				chunk.Refresh();
			}
		}

		public void RefreshCell(int x, int y)
		{
			GetCell(x, y).chunk.Refresh();
		}

		public void RefreshChunk(int index)
		{
			chunks[index].Refresh();
		}

		void AddCellToChunk(int x, int z, GameObject HexCell)
		{
			int chunkX = x / HexMetrics.chunkSizeX;
			int chunkZ = z / HexMetrics.chunkSizeZ;
			HexagonChunk chunk = chunks[chunkX + chunkZ * chunkCountX];

			int localX = x - chunkX * HexMetrics.chunkSizeX;
			int localZ = z - chunkZ * HexMetrics.chunkSizeZ;
			chunk.AddCell(localX + localZ * HexMetrics.chunkSizeX, HexCell);
		}

		public HexagonChunk GetChunk(int index)
		{
			return chunks[index];
		}

		//from worldspace.
		public HexagonCell GetCell(Vector3 position)
		{
			position = transform.InverseTransformPoint(position);
			HCoordinates coordinates = HCoordinates.FromPosition(position);
			int index = coordinates.X + coordinates.Z * width + coordinates.Z / 2;
			return cells[index];
		}

		public HexagonCell GetCell(int xOffset, int zOffset)
		{
			return cells[xOffset + zOffset * cellCountX];
		}

		public HexagonCell GetCell(int cellIndex)
		{
			return cells[cellIndex];
		}

		public void DestroyGrid()
		{
			if (chunks.Length > 0)
			{
				foreach (HexagonChunk hc in chunks)
				{
					GameObject.DestroyImmediate(hc.gameObject);

				}

				chunks = new HexagonChunk[0];
				cells = new HexagonCell[0];
			}
		}

		#region connect cell neighbors.
		void ConnectCells()
		{
			//Messy as all hell, but it works...
			for (int i = 0; i < cells.Length; i++)
			{

				try//left right
				{
					if (i % width == 0) //if on left edge
					{
						cells[i].AddNeighbor(cells[i + 1]);

						if (cells[i].coordinates.ToVector().z % 2 == 0)
						{ //if even

							try//up
							{
								cells[i].AddNeighbor(cells[i + width]);
							}
							catch { }
							try//down
							{
								cells[i].AddNeighbor(cells[i - width]);
								//cells[i].AddNeighbor(cells[i - width + 1]);
							}
							catch { }

						}
						else //odd
						{

							try//up
							{
								cells[i].AddNeighbor(cells[i + width]);
								cells[i].AddNeighbor(cells[i + width + 1]);
							}
							catch { }
							try//down
							{
								cells[i].AddNeighbor(cells[i - width]);
								cells[i].AddNeighbor(cells[i - width + 1]);
							}
							catch { }
						}

					}
					else if ((i + 1) % width == 0) //if on right edge
					{
						cells[i].AddNeighbor(cells[i - 1]);

						if (cells[i].coordinates.ToVector().z % 2 == 0)
						{ //if even

							try//up
							{
								cells[i].AddNeighbor(cells[i + width]);
								cells[i].AddNeighbor(cells[i + width - 1]);
							}
							catch { }
							try//down
							{
								cells[i].AddNeighbor(cells[i - width]);
								cells[i].AddNeighbor(cells[i - width - 1]);
							}
							catch { }
						}
						else //odd
						{
							try//up
							{
								cells[i].AddNeighbor(cells[i + width]);
							}
							catch { }
							try//down
							{
								cells[i].AddNeighbor(cells[i - width]);
								//cells[i].AddNeighbor(cells[i - width + 1]);
							}
							catch{}
						}

					}
					else
					{
						cells[i].AddNeighbor(cells[i - 1]);
						cells[i].AddNeighbor(cells[i + 1]);

						try //down
						{
							cells[i].AddNeighbor(cells[i - width]);
							cells[i].AddNeighbor(cells[i - width + 1]);
						}
						catch { }

						try //up
						{
							cells[i].AddNeighbor(cells[i + width]);
							cells[i].AddNeighbor(cells[i + width + 1]);
						}
						catch { }
					}
				}
				catch { }
			}
		}
		#endregion
	}
}
