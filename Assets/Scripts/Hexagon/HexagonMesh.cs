﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CSchrieverHexagonGrid
{

	[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
	public class HexagonMesh : MonoBehaviour
	{

		Mesh hexMesh;
		MeshCollider meshCollider;
		List<Vector3> vertices;
		List<int> triangles;
		List<Color> colors;
		
		void Awake()
		{
			GetComponent<MeshFilter>().mesh = hexMesh = new Mesh();
			meshCollider = gameObject.AddComponent<MeshCollider>();
			hexMesh.name = "Hex Mesh";
			vertices = new List<Vector3>();
			triangles = new List<int>();
			colors = new List<Color>();
		}

		//add triangles to the mesh for the cells.
		public void Triangulate(HexagonCell[] cells)
		{
			hexMesh.Clear();
			vertices.Clear();
			triangles.Clear();
			colors.Clear();

			foreach (HexagonCell cell in cells)
			{
				Triangulate(cell);
			}

			hexMesh.vertices = vertices.ToArray();
			hexMesh.colors = colors.ToArray();
			hexMesh.triangles = triangles.ToArray();
			hexMesh.RecalculateNormals();

			meshCollider.sharedMesh = hexMesh;
		}

		//add triangles to the mesh per cell.
		void Triangulate(HexagonCell cell)
		{
			Vector3 center = cell.transform.localPosition;

			//if the cell has a object/mesh
			if (cell.cellObject)
			{
				int count = vertices.Count;


				//GameObject selectedPrefab = cell.Biome.getRandomPOI().cellprefab;
				//Debug.Log("RandomCell");

				//selectedPrefab.GetComponent<MeshFilter>().sharedMesh
				foreach (Vector3 vert in cell.cellObject.GetComponent<MeshFilter>().sharedMesh.vertices)
				{
					vertices.Add(vert + center);
					colors.Add(cell.color);
				}

				foreach (int tri in cell.cellObject.GetComponent<MeshFilter>().sharedMesh.triangles)
				{
					triangles.Add(tri + count);
				}

				if (cell.cellObject.transform.childCount > 0)
				{
					if (cell.transform.childCount > 0)
					{
						for (int z = 0; z < cell.transform.childCount; z++)
						{
							Destroy(cell.transform.GetChild(z).gameObject);
						}
					}

					for (int x = 0; x < cell.cellObject.transform.childCount; x++)
					{
						//Debug.Log("Square" + cell.transform.name);
						Instantiate(cell.cellObject.transform.GetChild(x).gameObject, cell.transform);
					}
				}

			}
			else //else generate generic hexagon.
			{
				for (int i = 0; i < 6; i++)
				{
					AddTriangle(
						center,
						center + HexMetrics.corners[i],
						center + HexMetrics.corners[i + 1]
					);
					AddTriangleColor(Color.gray);
				}
			}
		}

		//void Triangulate(HexagonCell cell, GameObject prefabObject)
		//{
		//	Mesh prefabMesh = prefabObject.GetComponent<MeshFilter>().mesh;

		//	vertices.Add
		//}

		void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3)
		{
			int vertexIndex = vertices.Count;
			vertices.Add(v1);
			vertices.Add(v2);
			vertices.Add(v3);
			triangles.Add(vertexIndex);
			triangles.Add(vertexIndex + 1);
			triangles.Add(vertexIndex + 2);
		}

		void AddTriangleColor(Color color)
		{
			colors.Add(color);
			colors.Add(color);
			colors.Add(color);
		}

	}
}