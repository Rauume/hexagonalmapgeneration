﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CSchrieverGenerator
{
	
	public class Generator : MonoBehaviour
	{
		//public HexagonGrid grid;
		private List<GeneratedObject> generatedObjects = new List<GeneratedObject>();
		private int gridWidth;
		private int gridHeight;
		private int seed;

		[Header("Constant scale of the map")]
		public float entireMapScale = 75f;

		[Header("Individual generation scales")]
		public float heatMapScale = 5;
		public float heightMapScale = 5f;
		public float precipitationScale = 5f;
		public float seaScale = 5f;
		

		

		[Header("Water Settings")]
		public bool generateWater;
		public float seaLevel = 0.02f;
		public Biome oceanCell;


		public void Initialise(int seed, int gridWidth, int gridHeight)
		{
			Random.InitState(seed);
			this.seed = seed;
			this.gridHeight = gridHeight;
			this.gridWidth = gridWidth;

			GenerateMap();
		}

		//generates a cell for every point on the grid.
		private void GenerateMap()
		{

			for (int x = 0; x < gridWidth; x++)
			{				
				for (int y = 0; y < gridHeight; y++)
				{
					generateCell(x, y);
				}
			}
		}
		
		public void generateCell(int x, int y)
		{
			//checks if it is above sea level/not part of the sea map.
			if (SeaMap(x, y) < seaLevel && generateWater)
			{
				generatedObjects.Add(new GeneratedObject(oceanCell, seaLevel));
			}
			else //else, set the biome based on closest variables.
			{
				Biome desiredBiome = BiomeLibrary.FindCellByVector(new Vector3(PrecipitationMap(x, y), TemperatureMap(x, y), HeightMap(x, y)));
				generatedObjects.Add(new GeneratedObject(desiredBiome, HeightMap(x, y)));
			}
		}

		private float HeightMap(int x, int y)
		{
			// The origin of the sampled area in the plane.
			float xOrg = 100f * seed;
			float yOrg = 100f * seed;

			float xCoord = xOrg + x / entireMapScale * heightMapScale;
			float yCoord = yOrg + y / entireMapScale * heightMapScale;

			return Mathf.PerlinNoise(xCoord, yCoord);
		}

		//the different maps and how they generate from seeds and scales.
		#region Generation Maps
		private float TemperatureMap(int x, int y)
		{
			float xOrg = -100f * seed;
			float yOrg = -100f * seed;

			float xCoord = xOrg + x / entireMapScale * heatMapScale;
			float yCoord = yOrg + y / entireMapScale * heatMapScale;

			return Mathf.PerlinNoise(xCoord, yCoord);
		}

		private float PrecipitationMap(int x, int y)
		{
			float xOrg = -10f * seed;
			float yOrg = 100f * seed;

			float xCoord = xOrg + x / entireMapScale * precipitationScale;
			float yCoord = yOrg + y / entireMapScale * precipitationScale;

			return Mathf.PerlinNoise(xCoord, yCoord);
		}

		private float SeaMap(int x, int y)
		{
			float xOrg = -100f * seed;
			float yOrg = 10f * seed;

			float xCoord = xOrg + x / entireMapScale * seaScale;
			float yCoord = yOrg + y / entireMapScale * seaScale;

			return Mathf.PerlinNoise(xCoord, yCoord);
		}
		#endregion

		public GeneratedObject getObject(int xOffset, int zOffset)
		{
			return generatedObjects[xOffset + zOffset * gridWidth];
		}

		public void clearGeneratedObjects()
		{
			generatedObjects.Clear();
		}
	}

	//basic class that holds the biome information in the grid.
	public class GeneratedObject
	{
		public float altitude;
		public Biome biome;

		public GeneratedObject(Biome biome, float altitude)
		{
			this.biome = biome;
			this.altitude = altitude;
		}
	}
}