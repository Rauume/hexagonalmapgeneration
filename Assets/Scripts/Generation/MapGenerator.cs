﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//namespace CamMapGen
//{
//	public class MapGenerator : MonoBehaviour {

//		public HexagonGrid grid;

//		public int seed;
//		public float heatMapScale = 5;
//		public float heightMapScale = 5f;
//		public float precipitationScale = 5f;
//		public float seaScale = 5f;

//		[Header("Water Settings")]
//		public bool generateWater;
//		public float seaLevel = 0.02f;
//		public Biome oceanCell;

//		private void Start()
//		{
//			Random.InitState(seed);
//		}

//		private void Update()
//		{
//			if (Input.GetKeyDown(KeyCode.Space))
//			{
//				BiomeLibrary.markForRefresh = true;
//				GenerateMap();
//				Debug.Log("Refreshed");
//			}
//		}

//		public void GenerateMap()
//		{
//			float x = 0.0F;
//			while (x < HexagonGrid.width)
//			{
//				float y = 0.0F;

//				while (y < HexagonGrid.height)
//				{
//					generateCell(x, y);
				
//					y++;
//				}
//				x++;
//			}

//			if (generateWater)
//			{
//				//foreach (HexagonCell cell in grid.cells)
//				//{
//				//	fixWater(cell);
//				//}
//			}

//			//Debug.Log(grid.GetCell(0).neighbors.Count);
//			//fixWater(grid.GetCell(0));
//		}

//		public void generateCell(float x, float y)
//		{
				

//			if (SeaMap(x, y) < seaLevel && generateWater)
//			{
//				grid.GetCell((int)x, (int)y).OnGenerate(oceanCell);
//				grid.GetCell((int)x, (int)y).Altitude = seaLevel;
//			}
//			else
//			{
//				grid.GetCell((int)x, (int)y).Altitude = HeightMap(x, y);

//				grid.GetCell((int)x, (int)y).OnGenerate(BiomeLibrary.FindCellByVector(
//				new Vector3(PrecipitationMap(x, y), TemperatureMap(x, y), HeightMap(x, y))));
//			}
//		}

//		public void fixWater(HexagonCell cell)
//		{
//			foreach(HexagonCell neighbor in cell.neighbors)
//			{
//				if (cell.Biome == oceanCell)
//				{
//					if (neighbor.Altitude < seaLevel)
//					{
//						neighbor.Biome = oceanCell;
//						neighbor.Altitude = seaLevel;
//						fixWater(neighbor);
//					}
//				}
//			}
//		}

//		private float HeightMap(float x, float y)
//		{
//			// The origin of the sampled area in the plane.
//			float xOrg = 100f * seed;
//			float yOrg = 100f * seed;

//			float xCoord = xOrg + x / HexagonGrid.width * heightMapScale;
//			float yCoord = yOrg + y / HexagonGrid.height * heightMapScale;

//			return Mathf.PerlinNoise(xCoord, yCoord);
//		}

//		private float TemperatureMap(float x, float y)
//		{
//			float xOrg = -100f * seed;
//			float yOrg = -100f * seed;

//			float xCoord = xOrg + x / HexagonGrid.width * heatMapScale;
//			float yCoord = yOrg + y / HexagonGrid.height * heatMapScale;

//			return Mathf.PerlinNoise(xCoord, yCoord);
//		}

//		private float PrecipitationMap(float x, float y)
//		{
//			float xOrg = -10f * seed;
//			float yOrg = 100f * seed;

//			float xCoord = xOrg + x / HexagonGrid.width * precipitationScale;
//			float yCoord = yOrg + y / HexagonGrid.height * precipitationScale;

//			return Mathf.PerlinNoise(xCoord, yCoord);
//		}
//		private float SeaMap(float x, float y)
//		{
//			float xOrg = -100f * seed;
//			float yOrg = 10f * seed;

//			float xCoord = xOrg + x / HexagonGrid.width * seaScale;
//			float yCoord = yOrg + y / HexagonGrid.height * seaScale;

//			return Mathf.PerlinNoise(xCoord, yCoord);
//		}
//	}
//}
