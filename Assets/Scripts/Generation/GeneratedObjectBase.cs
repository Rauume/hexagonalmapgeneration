﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CSchrieverGenerator
{

	public class GeneratedObjectBase : MonoBehaviour
	{
		//public float altitude;
		[SerializeField]
		protected Biome biome;
		[SerializeField]
		protected Color color;
		protected float altitude;
		
		public void OnGenerate(Biome biome, float height)
		{
			this.Biome = biome;
			this.Color = biome.biomeColor;
			//cellPrefab = this.biome.getRandomPOI();

			//Refresh();
		}

		
		public Biome Biome {
			get {
				return biome;
			}
			set {
				if (biome == value)
				{
					return;
				}
				biome = value;
				color = value.biomeColor;

				//Refresh();
			}
		}

		public Color Color {
			get {
				return color;
			}
			set {
				if (color == value)
				{
					return;
				}
				color = value;
				//Refresh();
			}
		}



		public float Altitude {
			get {
				return altitude;
			}
			set {
				if (altitude == value)
				{
					return;
				}
				altitude = value;
				Vector3 position = transform.localPosition;
				position.y = value * 20;
				transform.localPosition = position;

				//Refresh();
			}
		}
	}
}