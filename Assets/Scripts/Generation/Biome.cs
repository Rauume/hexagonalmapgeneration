﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CSchrieverGenerator
{

	[CreateAssetMenu(fileName = "Biome", menuName = "Terrain/Biome", order = 1)]
	public class Biome : ScriptableObject
	{

		public string biomeName;

		public float precipitation;
		public float temperature;
		public float altitude;
		float accumulatedWeight;

		public Color biomeColor;

		public List<BiomePrefab> BiomePrefabs;


		public Vector3 GetAsVector()
		{
			return new Vector3(precipitation, temperature, altitude);
		}

		public BiomePrefab getRandomPOI()
		{
			accumulatedWeight = 0;
			calculateAccumulatedWeight();

			float randomNo = Random.Range(0, accumulatedWeight);
			float total = 0;

			//Random rand = new Random();

			foreach (BiomePrefab bcp in BiomePrefabs)
			{
				randomNo = randomNo - bcp.accumulatedWeight;

				total += bcp.weight;

				if (total >= randomNo)
				{
					return bcp;
				}
			}

			//only occurs if no entries are present
			return null;
		}

		private void calculateAccumulatedWeight()
		{
			foreach (BiomePrefab bcp in BiomePrefabs)
			{
				bcp.accumulatedWeight = 0;

				accumulatedWeight += bcp.weight;
				//bcp.accumulatedWeight += accumulatedWeight;
			}
		}
	}

	[System.Serializable]
	public class BiomePrefab
	{
		public string comment;

		[Header("Biome terrain cell prefab")]
		public GameObject cellprefab;
		[Header("Percentage chance")]
		//[Range(0.0f, 100f)]
		public float weight = 0;

		[HideInInspector]
		public float accumulatedWeight = 0;
	}
}