﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CSchrieverGenerator
{

	public static class BiomeLibrary
	{

		private static Dictionary<string, Biome> cellObjects = new Dictionary<string, Biome>();
		public static bool markForRefresh = false;

		public static void Initialise()
		{
			Biome[] TempObjects = Resources.LoadAll<Biome>("Biomes");

			if (cellObjects.Count > 0)
				cellObjects.Clear();

			foreach (Biome tc in TempObjects)
			{
				cellObjects.Add(tc.biomeName, tc);
			}
		}

		//public static GameObject FindObject(string objID)
		//{
		//	if (cellObjects.Count < 1)
		//	{
		//		Initialise();
		//	}

		//	Biome newCellObject;
		//	if (cellObjects.TryGetValue(objID, out newCellObject))
		//	{
		//		//return newCellObject.CellObjectPrefab;
		//		return null;
		//	}
		//	else
		//	{
		//		return null;
		//	}
		//}

		public static Biome FindCellObject(string objID)
		{
			if (cellObjects.Count < 1)
			{
				Initialise();
			}

			Biome newCellObject;
			if (cellObjects.TryGetValue(objID, out newCellObject))
			{
				return newCellObject;
			}
			else
			{
				return null;
			}
		}

		public static Biome FindCellByTemperature(float value)
		{
			if (cellObjects.Count < 1)
			{
				Initialise();
			}

			//float previousABS = Mathf.Infinity;
			Biome previousTC;
			cellObjects.TryGetValue("Taiga", out previousTC);


			foreach (KeyValuePair<string, Biome> tc in cellObjects)
			{
				float difference = Mathf.Abs(tc.Value.temperature - value);

				if (difference < Mathf.Abs(previousTC.temperature - value))
				{
					previousTC = tc.Value;
					//previousABS = difference;
				}
			}

			//Debug.Log(previousTC.biomeName);
			return previousTC;
		}

		/// <summary>
		/// Structure vector: (Precipitation/rain, Temperature, Altitude)
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public static Biome FindCellByVector(Vector3 input)
		{
			if (cellObjects.Count < 1 || markForRefresh == true)
			{
				Initialise();
			}


			Biome previousBiome;
			float previousDifference = Mathf.Infinity;

			cellObjects.TryGetValue("Taiga", out previousBiome);

			//find closest biome
			foreach (KeyValuePair<string, Biome> newBiome in cellObjects)
			{
				//faster than using vector3.distance 
				//'avoids the expensive square root operation that happens under the hood by doing "Vector3.Distance".'				
				Vector3 directionToTarget = newBiome.Value.GetAsVector() - input;
				float dSqrToTarget = directionToTarget.sqrMagnitude;

				if (dSqrToTarget < previousDifference)
				{
					previousBiome = newBiome.Value;
					previousDifference = dSqrToTarget;
				}
			}

			return previousBiome;
		}

	}
}