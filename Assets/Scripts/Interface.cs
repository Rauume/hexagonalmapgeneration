﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CSchrieverGenerator;
using CSchrieverHexagonGrid;
using UnityEngine.UI;

public class Interface : MonoBehaviour
{
	public Generator generator;

	public HexagonGrid grid;

	public int seed;
	public int chunkWidth;
	public int chunkHeight;

	private int cellXCount;
	private int cellYCount;

	public bool visualiseHeight = true;

	public InputField seedInput;

	public InputField HeatMapScale;
	public InputField HeightMapScale;
	public InputField PrecipitationMapScale;
	public InputField SeaScale;
	public InputField SeaLevel;

	public InputField MapWidth;
	public InputField MapHeight;

	private void Start()
	{
		HeatMapScale.text = generator.heatMapScale.ToString();
		HeightMapScale.text = generator.heightMapScale.ToString();
		PrecipitationMapScale.text = generator.precipitationScale.ToString();
		SeaScale.text = generator.seaScale.ToString();
		SeaLevel.text = generator.seaLevel.ToString();

		MapWidth.text = chunkWidth.ToString();
		MapHeight.text = chunkHeight.ToString();
	}

	public void makeGrid()
	{
		assignMapScales();

		seed = int.Parse(seedInput.text);
		grid.DestroyGrid();
		generator.clearGeneratedObjects();

		//setup chunks size
		cellXCount = chunkWidth * HexMetrics.chunkSizeX;
		cellYCount = chunkHeight * HexMetrics.chunkSizeZ;		

		//begin making the biomes
		generator.Initialise(seed, cellXCount, cellYCount);
		//begin making the grid
		grid.CreateGrid(chunkWidth, chunkHeight);

		//connect them together,
		StartCoroutine("AssignCells");
	}

	protected void assignMapScales()
	{
		generator.heatMapScale = float.Parse(HeatMapScale.text);
		generator.heightMapScale = float.Parse(HeightMapScale.text);
		generator.precipitationScale = float.Parse(PrecipitationMapScale.text);
		generator.seaScale = float.Parse(SeaScale.text);
		generator.seaLevel = float.Parse(SeaLevel.text);

		chunkHeight = int.Parse(MapHeight.text);
		chunkWidth= int.Parse(MapWidth.text);


	}

	IEnumerator AssignCells()
	{
		int chunkcount = chunkHeight * chunkWidth;

		for (int i = 0; i < chunkcount; i++)
		{
			HexagonChunk chunk = grid.GetChunk(i);

			foreach (HexagonCell cell in chunk.cells)
			{
				cell.assignCellObject(generator.getObject(cell.coordinates.X, cell.coordinates.Z).biome.getRandomPOI().cellprefab);

				cell.gameObject.GetComponent<GeneratedObjectBase>().OnGenerate(
					generator.getObject(cell.coordinates.X, cell.coordinates.Z).biome,
					generator.getObject(cell.coordinates.X, cell.coordinates.Z).altitude);

				cell.color = generator.getObject(cell.coordinates.X, cell.coordinates.Z).biome.biomeColor;

				if (visualiseHeight)
				{
					cell.gameObject.transform.position += new Vector3(0, generator.getObject(cell.coordinates.X, cell.coordinates.Z).altitude * 20, 0);
				}
			}

			grid.RefreshChunk(i);
			yield return null;
		}	
	}
}
